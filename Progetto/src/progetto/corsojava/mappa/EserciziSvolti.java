package progetto.corsojava.mappa;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EserciziSvolti {

	public HashMap<Integer, ArrayList<Object>> creaArmadio() {

		return new HashMap<Integer, ArrayList<Object>>();
	}

	public HashMap<Integer, ArrayList<Object>> riempiArmadioConZeroElementi(
			HashMap<Integer, ArrayList<Object>> armadio) {

		for (int i = 1; i <= 6; i++) {
			armadio.put(i, new ArrayList<Object>());
		}
		return armadio;
	}

	public HashMap<Integer, ArrayList<Object>> riempiArmadioConElementi(HashMap<Integer, ArrayList<Object>> armadio) {
		List<Object> temporanea = new ArrayList<>();
		for (int i = 1; i <= 6; i++) {

			switch (i) {
			case 1:
				temporanea = armadio.get(i);
				temporanea.add(1);
				temporanea.add(10);
				temporanea.add(100);
				temporanea.add(20000);
				temporanea.add(900);
				break;
			case 2:
				temporanea = armadio.get(i);
				temporanea.add("r");
				temporanea.add("gsf");
				temporanea.add("asfs");
				temporanea.add("afssf");
				temporanea.add("asfdfs");

				break;
			case 3:
				temporanea = armadio.get(i);
				temporanea.add(1.3);
				temporanea.add(10.5);
				temporanea.add(100.8);
				temporanea.add(20000.7);
				temporanea.add(900.8);
				break;
			case 4:

				temporanea = armadio.get(i);
				temporanea.add(true);
				temporanea.add(false);
				temporanea.add(false);
				temporanea.add(true);
				temporanea.add(false);

				break;
				
			case 5:

				temporanea = armadio.get(i);
				temporanea.add("PAROLA 1 ");
				temporanea.add("PAROLA 2 ");
				temporanea.add("PAROLA 3 ");
				temporanea.add("PAROLA 4 ");
				temporanea.add("PAROLA 5 ");

				break;
				
			case 6:

				temporanea = armadio.get(i);
				temporanea.add("stringa"+1);
				temporanea.add("stringa"+2);
				temporanea.add("stringa"+3);
				temporanea.add("stringa"+4);
				temporanea.add("stringa"+5);

				break;
			default:
				break;
			}

		}
		return armadio;
	}

	public void stampa(HashMap<Integer, ArrayList<Object>> armadio) {

		for (int i = 1; i <=armadio.size(); i++) {
			List<Object> temporanea = armadio.get(i);
			System.out.println("-------------scomparto" +i +"-------------");
			for (Object object : temporanea) {
				
				System.out.println(object);
			}
			System.out.println("---------------------------------------");
		}
	}
}