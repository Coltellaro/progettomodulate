package progetto.corsojava.mappa;

import java.util.ArrayList;
import java.util.HashMap;

public class Start {

	public static void main(String[] args) {
		HashMap<Integer, ArrayList<Object>> mioArmadio = new HashMap<Integer, ArrayList<Object>>();
		EserciziSvolti es = new EserciziSvolti();
		mioArmadio = es.creaArmadio();
		mioArmadio = es.riempiArmadioConZeroElementi(mioArmadio);
		mioArmadio = es.riempiArmadioConElementi(mioArmadio);
		es.stampa(mioArmadio);

	}

}
