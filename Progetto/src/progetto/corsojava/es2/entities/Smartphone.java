package progetto.corsojava.es2.entities;


public class Smartphone {
	
	private String modello;
	private double dimensioneDisplay;
	private int capacitaBatteria;
	private String sistemaOperativo;
	private int memoria;
	private int ram;
	private int fotocameraA;
	private int fotocameraP;
	private double prezzo;
	public String getModello() {
		return modello;
	}
	public void setModello(String modello) {
		this.modello = modello;
	}
	public double getDimensioneDisplay() {
		return dimensioneDisplay;
	}
	public void setDimensioneDisplay(double dimensioneDisplay) {
		this.dimensioneDisplay = dimensioneDisplay;
	}
	public int getCapacitaBatteria() {
		return capacitaBatteria;
	}
	public void setCapacitaBatteria(int capacitaBatteria) {
		this.capacitaBatteria = capacitaBatteria;
	}
	public String getSistemaOperativo() {
		return sistemaOperativo;
	}
	public void setSistemaOperativo(String sistemaOperativo) {
		this.sistemaOperativo = sistemaOperativo;
	}
	public int getMemoria() {
		return memoria;
	}
	public void setMemoria(int memoria) {
		this.memoria = memoria;
	}
	public int getRam() {
		return ram;
	}
	public void setRam(int ram) {
		this.ram = ram;
	}
	public int getFotocameraA() {
		return fotocameraA;
	}
	public void setFotocameraA(int fotocameraA) {
		this.fotocameraA = fotocameraA;
	}
	public int getFotocameraP() {
		return fotocameraP;
	}
	public void setFotocameraP(int fotocameraP) {
		this.fotocameraP = fotocameraP;
	}
	public double getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}

	
	
}
