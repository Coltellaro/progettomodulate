package progetto.corsojava.es2.entities;

public class OutputAnalisiSmartphone {
	
	private String modello;
	private int punteggio;
	
	public String getModello() {
		return modello;
	}
	public void setModello(String modello) {
		this.modello = modello;
	}
	public int getPunteggio() {
		return punteggio;
	}
	public void setPunteggio(int punteggio) {
		this.punteggio = punteggio;
	}
	
	

}
