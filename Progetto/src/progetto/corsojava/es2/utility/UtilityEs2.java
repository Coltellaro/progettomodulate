package progetto.corsojava.es2.utility;

import progetto.corsojava.es2.entities.OutputAnalisiSmartphone;
import progetto.corsojava.es2.entities.Smartphone;

public class UtilityEs2 {

	public OutputAnalisiSmartphone comparatore(Smartphone smartphone) {
		OutputAnalisiSmartphone ritorno = new OutputAnalisiSmartphone();

		if (smartphone.getDimensioneDisplay() > 0.0 && smartphone.getDimensioneDisplay() < 4.0) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 0);

		} else if (smartphone.getDimensioneDisplay() > 4.0 && smartphone.getDimensioneDisplay() < 5.0) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 1);

		} else if (smartphone.getDimensioneDisplay() > 5.0 && smartphone.getDimensioneDisplay() < 5.5) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 2);

		} else if (smartphone.getDimensioneDisplay() > 5.5) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 3);
		}

		if (smartphone.getCapacitaBatteria() > 0.0 && smartphone.getCapacitaBatteria() < 2000) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 0);

		} else if (smartphone.getCapacitaBatteria() > 2000 && smartphone.getCapacitaBatteria() < 3000) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 1);

		} else if (smartphone.getCapacitaBatteria() > 3000 && smartphone.getCapacitaBatteria() < 4000) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 2);

		} else if (smartphone.getCapacitaBatteria() > 4000) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 3);
		}

		if (smartphone.getMemoria() > 0.0 && smartphone.getMemoria() < 16) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 0);

		} else if (smartphone.getMemoria() > 16 && smartphone.getMemoria() < 64) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 1);

		} else if (smartphone.getMemoria() > 64 && smartphone.getMemoria() < 128) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 2);

		} else if (smartphone.getMemoria() > 128) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 3);
		}
	
		if (smartphone.getRam() > 0.0 && smartphone.getRam() < 2) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 0);

		} else if (smartphone.getRam() > 2 && smartphone.getRam() < 4) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 1);

		} else if (smartphone.getRam() > 4 && smartphone.getRam() < 6) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 2);

		} else if (smartphone.getRam() > 6) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 3);
		}
		
		if (smartphone.getFotocameraA() > 0.0 && smartphone.getFotocameraA() < 6) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 0);

		} else if (smartphone.getFotocameraA() > 6 && smartphone.getFotocameraA() < 8) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 1);

		} else if (smartphone.getFotocameraA() > 8 && smartphone.getFotocameraA() < 12) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 2);

		} else if (smartphone.getFotocameraA() > 12) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 3);
		}
		if (smartphone.getFotocameraP() > 0.0 && smartphone.getFotocameraP() < 12) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 0);

		} else if (smartphone.getFotocameraP() > 12 && smartphone.getFotocameraP() < 24) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 1);

		} else if (smartphone.getFotocameraP() > 24 && smartphone.getFotocameraP() < 36) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 2);

		} else if (smartphone.getFotocameraP() > 36) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 3);
		}
		if (smartphone.getPrezzo() > 1000 ) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 0);

		} else if (smartphone.getPrezzo() > 800 && smartphone.getPrezzo() < 1000) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 1);

		} else if (smartphone.getPrezzo() > 200 && smartphone.getPrezzo() < 800) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 2);

		} else if (smartphone.getPrezzo() > 0 && smartphone.getPrezzo() < 200) {
			ritorno.setPunteggio(ritorno.getPunteggio() + 3);
		}
		
		System.out.print( ritorno.getModello() +"\n" + "" +  "\n " +  ritorno.getPunteggio());
		for (int i = 0; i <ritorno.getPunteggio(); i++) {
			System.out.print("*");
			
		}
		return ritorno;
		
	}
	
	
	
	
}