package progetto.corsojava.es2.gestione;

import java.util.ArrayList;
import java.util.List;

import progetto.corsojava.es2.entities.OutputAnalisiSmartphone;
import progetto.corsojava.es2.entities.Smartphone;
import progetto.corsojava.es2.utility.UtilityEs2;

public class GestioneEs2 {
	
	public List<Smartphone> listaSmartphoneDatestare (){
	
		List<Smartphone> list = new ArrayList<>();
		//telefono 1
		Smartphone s1 = new Smartphone();
		s1.setModello("samsung");
		s1.setCapacitaBatteria(2000);
		s1.setDimensioneDisplay(3.2);
		s1.setFotocameraP(32);
		s1.setFotocameraA(7);
		s1.setMemoria(500);
		//..........
		//telefono 2
		Smartphone s2 = new Smartphone();
		s2.setModello("iphone");
		s2.setCapacitaBatteria(2000);
		s2.setDimensioneDisplay(3.2);
		s2.setFotocameraP(50);
		s2.setFotocameraA(10);
		s2.setMemoria(700);
		//............
		//telefono 3
		Smartphone s3 = new Smartphone();
		s3.setModello("hawey");
		s3.setCapacitaBatteria(2000);
		s3.setDimensioneDisplay(3.2);
		s3.setFotocameraP(40);
		s3.setFotocameraA(8);
		s3.setMemoria(100);
		
		//............
		list.add(s1);
		list.add(s2);
		list.add(s3);
		
		return list;
	
	}
	
	public Smartphone dammiIlMigliore() {
		UtilityEs2 utility = new UtilityEs2();
		List<OutputAnalisiSmartphone> analisi = new ArrayList<>();
		List<Smartphone>listaDiSmartphone = listaSmartphoneDatestare();
		
		for (Smartphone smartphone : listaDiSmartphone) {
			OutputAnalisiSmartphone analisiSF = utility.comparatore(smartphone);
			if(analisiSF != null)
				analisi.add(analisiSF);
			System.out.println();
			}
		
		return null;
	}

}
