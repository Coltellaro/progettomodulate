package progetto.corsojava.es1.gestione;

import java.util.Arrays;
import java.util.List;

import progetto.corsojava.es1.utility.UtilityEs1;

public class GestioneEs1 {

	public void usareLeListe() {

		UtilityEs1 u = new UtilityEs1();

		List<Integer> listaGenerata = u.generaLista(20);
		u.formattaLaStampa("LISTA", "generato", listaGenerata, null);

		listaGenerata = u.dividiLista(listaGenerata);
		u.formattaLaStampa("LISTA", "diviso", listaGenerata, null);

		Integer ritorno = u.dammiPrimoElemento(listaGenerata);
		u.formattaLaStampa("PRIMO", "Invertito", null, ritorno);

		listaGenerata = u.invertiLista(listaGenerata);
		u.formattaLaStampa("LISTA", "Invertito", listaGenerata, null);

		listaGenerata = u.numeriDispari(listaGenerata);
		u.formattaLaStampa("LISTA", "selezionato", listaGenerata, null);

		Integer ritornoUno = u.dammiUltimoElemento(listaGenerata);
		u.formattaLaStampa("ULTIMO", "invertito", null, ritornoUno);

		listaGenerata = u.primoEUltimoElemento(listaGenerata);
		u.formattaLaStampa("LISTA", "inserito", listaGenerata, null);

		listaGenerata = u.dispari(listaGenerata);
		u.formattaLaStampa("LISTA", "selezionato", listaGenerata, null);

		listaGenerata = u.generaLista(20);
		u.formattaLaStampa("LISTA", "generato", listaGenerata, null);
		listaGenerata = u.numerialternati(listaGenerata, 3);
		u.formattaLaStampa("LISTA", "numeriAlternati", listaGenerata, null);

		int leonardo = u.centrale(listaGenerata);
		u.formattaLaStampa("CENTRALE", "elementoCentrale", null, leonardo);

		listaGenerata = u.generaLista(20);
		u.formattaLaStampa("LISTA", "generato", listaGenerata, null);
		listaGenerata = u.numeriPari(listaGenerata);
		u.formattaLaStampa("LISTA", "pari", listaGenerata, null);

		listaGenerata = u.generaLista(20);
		u.formattaLaStampa("LISTA", "generato", listaGenerata, null);
		listaGenerata = u.piuDieci(listaGenerata);
		u.formattaLaStampa("LISTA", "piu dieci", listaGenerata, null);

		listaGenerata = u.generaLista(20);
		u.formattaLaStampa("LISTA", "generato", listaGenerata, null);
		listaGenerata = u.ordineInvertito(listaGenerata);
		u.formattaLaStampa("LISTA", "incolla", listaGenerata, null);

		listaGenerata = u.generaLista(10);
		u.formattaLaStampa("LISTA", "generato", listaGenerata, null);
		listaGenerata = u.invertito(listaGenerata);
		u.formattaLaStampa("LISTA", "INVERTI", listaGenerata, null);

		listaGenerata = u.generaLista(10);
		u.formattaLaStampa("LISTA", "generato", listaGenerata, null);
		listaGenerata = u.ritornoListaConTuttiElementiUgualiUltimo(listaGenerata);
		u.formattaLaStampa("LISTA", "RITORNO", listaGenerata, null);

		listaGenerata = u.generaLista(10);
		u.formattaLaStampa("LISTA", "generato", listaGenerata, null);
		List<Boolean> bool = u.ritornoVeroFalso(listaGenerata);
		u.formattaLaStampa("LISTA", bool);

		List<String> list = Arrays.asList("A", "B", "C");
		List<String> stringhe = u.parole(list);
		for (int i = 0; i < stringhe.size(); i++) {
			System.out.println(list.get(i));
		}

		List<String> list1 = Arrays.asList("Dodo", "Leonarda", "Gianmarie");
		List<String> stringhe1 = u.paroleUltima(list1);
		for (int i = 0; i < stringhe1.size(); i++) {
			System.out.println(stringhe1.get(i));
   
		}
		listaGenerata = u.generaLista(10);
		
		List<Double> conv = u.convertitore(listaGenerata);
		for (int i = 0; i < conv.size(); i++) {
			System.out.println(conv.get(i));
		}
		

	}
}
