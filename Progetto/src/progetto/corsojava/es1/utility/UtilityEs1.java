package progetto.corsojava.es1.utility;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class UtilityEs1 {

	/*
	 * Questo metodo prende in input il valore della dimensione della lista e genera
	 * in modo ramdom i valori per riempirla. Ritorna una lista
	 */
	public List<Integer> generaLista(int fineFor) {
		List<Integer> toreturn = new ArrayList<>();
		if (fineFor > 0) {
			for (int i = 0; i < fineFor; i++) {
				int x = (int) (Math.random() * 100);
				toreturn.add(x);
			}
		}
		return toreturn;
	}

	/*
	 * Questo metodo prende in input una lista e la divide a met� Ritorna una lista
	 */
	public List<Integer> dividiLista(List<Integer> listaInInput) {

		int lunghezza = listaInInput.size() / 2;
		List<Integer> ritorno = new ArrayList<>();
		for (int i = 0; i < lunghezza; i++) {
			ritorno.add(listaInInput.get(i));
		}
		return ritorno;
	}

	/*
	 * Questo metodo prende in input una lista e la stamapa Non ritorna nulla
	 */
	public void stampa(List<Integer> listaInInput) {
		for (int i = 0; i < listaInInput.size(); i++) {
			System.out.println(listaInInput.get(i));
		}

	}

	/*
	 * Questo metodo prende in input una lista e ritorna il primo elemento
	 */
	public Integer dammiPrimoElemento(List<Integer> listaInInput) {
		return listaInInput.get(0);
	}

	/*
	 * Questo metodo prende in input una lista e la inverte Ritorna una lista
	 */
	public List<Integer> invertiLista(List<Integer> listaInInput) {
		List<Integer> ritorno = new ArrayList<>();
		for (int i = listaInInput.size() - 1; i >= 0; i--) {
			ritorno.add(listaInInput.get(i));
		}
		return ritorno;
	}

	/*
	 * Questo metodo prende in input una lista, un integer e una parola se la lista
	 * � null stampa il valore, viceversa stampa la lista con la parola passata in
	 * input
	 */
	public void formattaLaStampa(String cosaDevoStampare, String parola, List<Integer> listaInInput, Integer valore) {
		switch (cosaDevoStampare) {
		case "LISTA":
			System.out.println("HO " + parola.toUpperCase() + " LA LISTA ");
			stampa(listaInInput);
			System.out.println("--------------- ");

			break;
		case "PRIMO":
			System.out.println("RITORNO IL PRIMO ELEMENTO : " + valore);
			System.out.println("--------------- ");

			break;
		case "ULTIMO":
			System.out.println("RITORNO L'ULTIMO ELEMENTO : " + valore);
			System.out.println("--------------- ");

		case "CENTRALE":
			System.out.println("RITORNA L'ELEMENTO CENTRALE : " + valore);
			System.out.println("--------------- ");

		default:
			break;
		}

	}

	/*
	 * questo metodo seleziona in un altra lista solo i numeri dispari ritorna una
	 * nuova lista
	 *
	 */
	public List<Integer> numeriDispari(List<Integer> listaInInput) {

		List<Integer> ritorno = new ArrayList<>();
		for (int i = 0; i < listaInInput.size(); i = i + 2) {
			ritorno.add(listaInInput.get(i));
		}
		return ritorno;

	}

	/*
	 * Questo metodo prende in input una lista e ritorna l'ultimo elemento
	 * dammiUltimoElemento
	 *
	 */
	public Integer dammiUltimoElemento(List<Integer> listaInInput) {
		return listaInInput.get(listaInInput.size() - 1);

	}

	public List<Integer> primoEUltimoElemento(List<Integer> listaInInput) {

		Integer a = dammiPrimoElemento(listaInInput);
		Integer b = dammiUltimoElemento(listaInInput);
		List<Integer> primoEUltimoElemento = new LinkedList<Integer>();
		primoEUltimoElemento.add(a);
		primoEUltimoElemento.add(b);
		return primoEUltimoElemento;
	}

	public List<Integer> dispari(List<Integer> listaInInput) {
		List<Integer> ritorno = new ArrayList<>();
		for (int i = 0; i < listaInInput.size(); i++) {

			if (listaInInput.get(i) % 2 != 0) {
				ritorno.add(listaInInput.get(i));

			}
		}

		return ritorno;
	}

	public List<Integer> numerialternati(List<Integer> listaInInput, int valore) {

		List<Integer> ritorno = new ArrayList<>();
		for (int i = 0; i < listaInInput.size(); i = i + valore) {

			ritorno.add(listaInInput.get(i));
		}
		return ritorno;
	}

	public int centrale(List<Integer> ListaInInput) {
		int result = ListaInInput.size() / 2;
		return ListaInInput.get(result);
	}

	public List<Integer> numeriPari(List<Integer> ListaInInput) {

		List<Integer> ritorno = new ArrayList<>();
		for (int i = 1; i < ListaInInput.size(); i = i + 2) {
			ritorno.add(ListaInInput.get(i));
		}
		return ritorno;

	}

	public List<Integer> piuDieci(List<Integer> ListaInInput) {

		List<Integer> ritorno = new ArrayList<>();

		for (int i = 0; i < ListaInInput.size(); i++) {
			ritorno.add(ListaInInput.get(i) + 10);

		}
		return ritorno;

	}
//hxvhsjsksg
//kzahbvzhaixby

	public List<Integer> ordineInvertito(List<Integer> ListaInInput) {

		List<Integer> ritorno = new ArrayList<>();
		List<Integer> lista2 = ListaInInput;

		ritorno.addAll(lista2);
		ritorno.addAll(ListaInInput);

		return ritorno;

	}

	public List<Integer> invertito(List<Integer> ListaInInput) {

		List<Integer> ritorno = new ArrayList<>();
		List<Integer> lista2 = ListaInInput;

		ritorno.addAll(ListaInInput);

		for (int i = lista2.size() - 1; i >= 0; i--) {
			ritorno.add(lista2.get(i));
		}
		return ritorno;

	}

	public List<Integer> ritornoListaConTuttiElementiUgualiUltimo(List<Integer> ListaInInput) {

		List<Integer> ritorno = new ArrayList<>();

		for (int i = 0; i < ListaInInput.size() - 1; i++) {
			ritorno.add(ListaInInput.get(ListaInInput.size() - 1) + 1);
		}
		return ritorno;

	}

	public List<Boolean> ritornoVeroFalso(List<Integer> ListaInInput) {

		List<Boolean> ritorno = new ArrayList<>();

		for (int i = 0; i < ListaInInput.size(); i++) {

			if (ListaInInput.get(i) % 2 != 0) {
				ritorno.add(false);

			} else
				ritorno.add(true);
		}

		return ritorno;
	}

	public void formattaLaStampa(String parola, List<Boolean> listaInInput) {

		System.out.println("HO " + parola.toUpperCase() + " LA LISTA ");
		stampaBool(listaInInput);
		System.out.println("--------------- ");

	}

	public void stampaBool(List<Boolean> listaInInput) {
		for (int i = 0; i < listaInInput.size(); i++) {
			System.out.println(listaInInput.get(i));
		}

	}

	public List<String> parole(List<String> parole) {

		String parola = null;
		List<String> stringhe = new ArrayList<>();

		for (int i = 0; i < parole.size(); i++) {
			parola = parole.get(i);
			char c = parola.charAt(0);
			stringhe.add(c + "");

		}

		return stringhe;
	}

	public List<String> paroleUltima(List<String> parole) {

		String parola = null;
		List<String> stringhe = new ArrayList<>();

		for (int i = 0; i < parole.size(); i++) {
			parola = parole.get(i);
			char c = parola.charAt(parola.length() - 1);
			stringhe.add(c + "");

		}

		return stringhe;
	}

	public List<Double> convertitore(List<Integer> convertito) {

		List<Double> lista2 = new ArrayList<>();

		for (int i = 0; i < convertito.size(); i++) {
			lista2.add(new Double(convertito.get(i)));
		}
		return lista2;

	}
}
